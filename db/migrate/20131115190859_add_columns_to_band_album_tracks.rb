class AddColumnsToBandAlbumTracks < ActiveRecord::Migration
  def change
    add_column :bands, :name, :string
    add_column :albums, :title, :string
    add_column :albums, :type, :string
    add_column :albums, :band_id, :integer
  end

end
