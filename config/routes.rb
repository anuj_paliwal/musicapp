MusicApp::Application.routes.draw do
  resources :users
  resource :sessions
  resources :bands do
    resources :albums, :except => :show
  end

  resources :albums

  resources :tracks
end
