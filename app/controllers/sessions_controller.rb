class SessionsController < ApplicationController

  def create
    @user = User.find_by_credentials(
      params[:user][:email],
      params[:user][:secret]
    )
    if @user.nil?
      flash[:notice] = "Email not in databse"
      redirect_to new_sessions_url
    elsif @user.save
      @user.reset_session_token!
      session[:session_token] = @user.session_token
      flash[:notice] = "You have successfully logged in"
      redirect_to user_url(@user)
    else
      #this part doesn't work properly
      flash[:notice] = "Invalid Login"
      redirect_to new_sessions_url
    end

  end

  def destroy
    session[:session_token] = nil if self.logged_in?
    redirect_to new_sessions_url
  end

end
