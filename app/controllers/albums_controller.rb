class AlbumsController < ApplicationController

  def index
    @bands = Band.all
    @albums = Album.all
  end

  def new
    @album = Album.new
    @bands = Band.all
  end

  def create
    @album = Album.new(params[:album])
    if @album.save
      flash[:notice] = "Album successfully saved"
      redirect_to bands_url
    else
      flash[:errors] = @album.errors.full_messages
      redirect_to new_album_url
    end

  end
end
