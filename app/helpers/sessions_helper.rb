module SessionsHelper
  attr_accessor :current_user

  def current_user
    return nil if session[:session_token].nil?
    User.find_by_session_token(session[:session_token])
  end

  def logged_in?
    self.current_user.session_token == session[:session_token]
  end

  def log_in_user!(user)
    user.reset_session_token!
    session[:session_token] = user.session_token
  end

end
