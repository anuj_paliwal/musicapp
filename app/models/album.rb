class Album < ActiveRecord::Base
  attr_accessible :band_id, :title, :album_type
  validates :album_type, :presence => true, :inclusion => %w(S L) #studio/live
  validates :band_id, :presence => true
  validates :title, :presence => true

  belongs_to(
    :band,
    :class_name => "Band",
    :foreign_key => :band_id,
    :primary_key => :id
  )

  # has_many(
#     :tracks,
#     :class_name => "Track",
#     :foreign_key => :album_id,
#     :primary_key => :id,
#     :dependent => :destroy
#   )
end
