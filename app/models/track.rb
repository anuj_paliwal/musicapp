class Track < ActiveRecord::Base
  attr_accessible :title, :album_id, :track_type
  validates :track_type, :inclusion => %w(B R) #bonus/regular

  belongs_to(
    :album,
    :class_name => "Album",
    :foreign_key => :album_id,
    :primary_key => :id
  )
end
