class User < ActiveRecord::Base
  attr_accessible :email, :password_digest, :secret, :session_token
  attr_reader :secret
  validates :secret, :length => { :minimum => 6 }, :allow_nil => true
  before_validation :ensure_session_token

  def self.generate_session_token
    SecureRandom::urlsafe_base64(16)
  end

  def self.find_by_credentials(email, secret)
    user = User.find_by_email(email)
    if user
      user.is_password?(secret) ? user : nil
    end
  end

  def reset_session_token!
    self.session_token = SecureRandom::urlsafe_base64(16)
    self.save!
  end


  def secret=(secret)
    @secret = secret
    self.password_digest = BCrypt::Password.create(secret)
  end

  def is_password?(secret)
    BCrypt::Password.new(self.password_digest).is_password?(secret)
  end

  def ensure_session_token
    self.session_token ||= User.generate_session_token
  end

end
